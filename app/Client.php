<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {
    protected $table = 'clients';
    protected $fillable = [
        'type',
        'document', 
        'business_name', 
        'fiscal_address',
        'install_address',
        'install_hours',
        'number_vehicles', 
        'id_vehicle' ,
        'user' ,
        'password',
        'price_gps' ,
        'price_plataform',
        'is_rented',
        'rent_expiration',
        'purchase_order' ,
        'payment_method',
        'reference'
    ];

}