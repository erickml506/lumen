<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class UsersController extends Controller
{
    //get all users
    function getAllUsers(Request $request)
    {
        //validation only json request
        if($request->isJson()){  
            $users = User::all();
            return response()->json($users, 200);
        }
        return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    //get only one user
    public function getUser(Request $request, $id)
    {
        if ($request->isJson()) {
            $user = User::find($id);
            if($user){
                return response()->json($user, 200);
            }else{
                return response()->json(['error' => "No content"], 406);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401, []);
        }

    }

    //create a user
    function createUser(Request $request)
    {
        //validation only json request
        if($request->isJson()){
            try{ 
                $data = $request->json()->all();
                //validation if user to create exists
                $findUser = User::where('email', $data['email'])->first();
                if($findUser){
                    return response()->json(['error' => "Duplicated user. Please change data"], 406);
                }else{
                    $user = User::create([
                        'name'=> $data['name'],
                        'apellidos'=> $data['apellidos'],
                        'email'=> $data['email'],
                        'password'=> Hash::make($data['password']),
                        'api_token' => str_random(60)
                    ]);
                    return response()->json($user, 201);
                }
            } catch(ModelNotFoundException $e){
                return response()->json(['error' => "No content"], 406);
            }
        }
        return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    //update users
    function updateUser(Request $request, $id){
        if ($request->isJson()) {

            //instance of user with specific id
            $user = User::find($id);
            if($user){
                //get data from request
                $data = $request->json()->all();
                //put data in the instance
                $user->name = $data['name'];
                $user->apellidos = $data['apellidos'];
                $user->email = $data['email'];
                if($data['password']){
                    $user->password = Hash::make($data['password']);
                }else{
                    $user->password = $user->password;
                }
                $user->save();
                return response()->json($user, 200);
            }else{
                return response()->json(['error' => "No content"], 406);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401, []);
        }
    }

    //detele a user
    public function deteleUser(Request $request, $id)
    {
        if ($request->isJson()) {
            try {
                $user = User::find($id);
                //validation if user exists
                if($user){
                    $user->delete();
                    return response()->json($user, 200);
                }else{
                    return response()->json(['error' => 'No content'], 406);
                }         
            } catch (ModelNotFoundException $e) {
                return response()->json(['error' => 'No content'], 406);
            }

        } else {
            return response()->json(['error' => 'Unauthorized'], 401, []);
        }
    }

    //get a token for test validation
    function getToken(Request $request){
        if($request->isJson()){
            try{
                $data = $request->json()->all();
                $user = User::where('email', $data['email'])->first();
                if($user && Hash::check($data['password'], $user->password)){
                    return response()->json($user, 200);
                }else{
                    response()->json(['error'=>'No content'], 406);
                }
            }catch(ModelNotFoundException $e){
                return response()->json(['error'=>'No content'], 406);
            }
        }
        return response()->json(['error'=>'Unauthorized'], 401);

    }
}
