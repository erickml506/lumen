<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
    protected $table = 'contacts';
    protected $fillable = [
        'id_client', 'name', 'phone', 'relationship', 'email', 'type'
    ];

}