<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model {
    protected $table = 'vehicles';
    protected $fillable = [
        'imei', 
        'name', 
        'brand', 
        'model', 
        'plate', 
        'phone', 
        'simcard',
        'protocol',
        'client_id', 
    ];

}