<?php

/*
|--------------------------------------------------------------------------
| Login
|--------------------------------------------------------------------------
|
| Here is where the user is validate and get api_token, to can make
| request CRUD.This route is only for test this service, cuz' the real
| api_token is goint to get from session storage
*/
$router->post('/users/login', ['uses' => 'UsersController@getToken']);

/*
|--------------------------------------------------------------------------
| CRUD USER
|--------------------------------------------------------------------------
|
| Here is the crud of the application, create,read,update and delete,
| just works if the user if authenticated
|
*/
$router->group(['middleware'=> ['auth']], function () use ($router){
    $router->group(['prefix'=> 'api/v1'], function () use ($router){
        $router->get('/users', ['uses' => 'UsersController@getAllUsers']);
        $router->get('/users/{id}', ['uses' => 'UsersController@getUser']);
        $router->post('/users', ['uses' => 'UsersController@createUser']);
        $router->put('/users/{id}', ['uses' => 'UsersController@updateUser']);
        $router->delete('/users/{id}', ['uses' => 'UsersController@deteleUser']);
    });
});



