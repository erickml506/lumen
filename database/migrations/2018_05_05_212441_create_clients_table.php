<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->increments('id');
            $table->string('type');
            $table->string('document')->unique();
            $table->string('business_name');
            $table->string('fiscal_address');
            $table->string('install_address');
            $table->string('install_hours');
            $table->integer('number_vehicles');
            $table->string('user');
            $table->string('password');
            $table->decimal('price_gps', 8, 2);
            $table->decimal('price_plataform', 8, 2);
            $table->boolean('is_rented');
            $table->date('rent_expiration');
            $table->string('purchase_order');
            $table->string('payment_method');
            $table->text('reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
