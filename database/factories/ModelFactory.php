<?php
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'apellidos' => $faker->sentence(2, false),
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make(str_random(10)),
        'remember_token' => str_random(10),
        'api_token' => str_random(60)
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    $data = [
        'type' => $faker->randomElement(["Company", "Private"]),
        'fiscal_address' => $faker->address,
        'install_address' => $faker->streetAddress,
        'install_hours'=> $faker->time().' to '.$faker->time(),
        'number_vehicles'=> $faker->numberBetween(1, 200),
        'user' => $faker->unique()->userName,
        'password'=> $faker->password,
        'price_gps' => $faker->randomFloat(2, 1, 100),
        'price_plataform' => $faker->randomFloat(2, 1, 100),
        'is_rented' => $faker->randomElement([true, false]),
        'rent_expiration' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'purchase_order'=> str_random(5),
        'payment_method'=>$faker->randomElement(["Cash", "Visa", "Down payment", "Transfer"]),
        'reference'=> $faker->secondaryAddress
    ];
    if($data['type'] == 'Private'){
        $faker->addProvider(new Faker\Provider\es_PE\Person($faker));
        $data['document'] = $faker->unique()->dni;
        $data['business_name'] = $faker->name;
    }else{
        $faker->addProvider(new Faker\Provider\en_ZA\Company($faker));
        $data['document'] = str_replace('/', "", $faker->unique()->companyNumber);
        $data['business_name'] = $faker->company();
    }
    return $data;
});

$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    return [
        'id_client' => $faker->randomElement([1, 2, 3, 4,5]),
        'name' => $faker->name,
        'phone' => '949280644',
        'relationship' => $faker->randomElement(['Layer', 'Doctor', 'Brother', "Myself"]),
        'email' => $faker->unique()->safeEmail,
        'type' => $faker->randomElement(['Owner', 'Billing', 'Support'])
    ];
});

$factory->define(App\Vehicle::class, function (Faker\Generator $faker) {
    $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
    $faker->addProvider(new Faker\Provider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new \carfaker\provider\Car($faker));
    $faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
    return [
        'imei' => $faker->unique()->randomNumber(8), 
        'name' => $faker->car, 
        'brand'=> $faker->unique()->brand, 
        'model'=> $faker->randomNumber(8), 
        'plate' => $faker->unique()->jpjNumberPlate, 
        'phone'=> $faker->cellphone, 
        'simcard'=> $faker->randomNumber(8),
        'protocol'=>'AAEE',
        'client_id'=> $faker->randomElement([1, 2, 3]) 
    ];
});